import * as vscode from 'vscode';
import { ExtensionTerminalOptions, EventEmitter, Event, Disposable, Terminal, TerminalDimensions } from 'vscode';
import * as fs from 'fs';
import { ReadStream } from 'fs';
import { Buffer } from 'buffer';
import * as child_process from 'child_process';

const execSyncOptions: child_process.ExecSyncOptionsWithStringEncoding = { encoding: 'utf8' };

export enum DataBits {
	Seven = 7,
	Eight = 8
}

export enum StopBits {
	None = 0,
	One = 1,
	Two = 2,
	OnePointFive = 1.5
}

export enum Parity {
	None = "n",
	Odd = "o",
	Even = "e",
	Mark = "m",
	Space = "s"
}

export enum RequestToSend {
	// A constant signal to show the terminal is ready to send data.
	On = "on",
	// No signal.
	Off = "off",
	// A handshake signal between the two terminals.
	Handshake = "hs",
	// provides a way to toggle between ready and not ready states.
	Toggle = "tg"
}

const BUFFER_SIZE: number = 64;

enum TerminalCodes {
	CLEAR = '\x1b[2J\x1b[3J\x1b[;H',
	ARROW_UP = '\x1b[A',
	ARROW_DOWN = '\x1b[B',
	ARROW_RIGHT = '\x1b[C',
	ARROW_LEFT = '\x1b[D',
	DELETE_CHAR = '\x1b[P',
	DELETE = '\x1b[3~',
	BACKSPACE = '\x7f',
	RETURN = '\r'
}

export class SerialConnection implements Disposable {

	private _fileDescriptor: number = 0;
	private _readStream: ReadStream | undefined;
	private _buffer: Buffer = Buffer.alloc(BUFFER_SIZE);
	private _isRunning: boolean = false;

	private _writeEmitter = new vscode.EventEmitter<string>();
	private _dimensionsEmitter = new vscode.EventEmitter<TerminalDimensions | undefined>();
	private _terminal: Terminal | undefined = undefined;

	public get isClosed(): boolean { return !this._isRunning };

	public constructor(
		private readonly _context: vscode.ExtensionContext,
		private readonly _portNumber: number = 0,
		private readonly _baud: number = 115200,
		private readonly _dataBits: DataBits = DataBits.Eight,
		private readonly _stopBits: StopBits = StopBits.One,
		private readonly _parity: Parity = Parity.None,
		private readonly _requestToSend: RequestToSend = RequestToSend.On
	) {
	}

	public getExtensionTerminalOptions(): ExtensionTerminalOptions {
		const $this = this;
		return {
			name: `Serial Port ${this._portNumber}`,
			pty: {
				onDidWrite: $this._writeEmitter.event,
				onDidOverrideDimensions: $this._dimensionsEmitter.event,
				open: () => { },
				close: () => { $this.close(); },
				handleInput: (data: string) => {
					if (data === TerminalCodes.RETURN) { // Enter
					} else if (data === TerminalCodes.BACKSPACE) {
					} else if (data === TerminalCodes.DELETE) {
						$this.clearTerminal();
					} else if (data === TerminalCodes.ARROW_UP) {
					} else if (data === TerminalCodes.ARROW_DOWN) {
					} else if (data === TerminalCodes.ARROW_RIGHT) {
					} else if (data === TerminalCodes.ARROW_LEFT) {
					} else { $this.write(data); }
				}
			}};
	}

	public static async list(): Promise<number[]> {
		let output = child_process.execSync("mode", execSyncOptions);
		/*
		let output = `
Status von Gerät COM1:
---------------------
    ...

Status von Gerät COM2:
---------------------
    ...
`;
		*/

		let regex = /COM(\d)+:/g;
		let matches: number[] = [];

		for (let match = regex.exec(output); match !== null; match = regex.exec(output)) {
			let port = parseInt(match[1]);
			if (!matches.includes(port)) {
				matches.push(port);
			}
		}

		return matches;
	}

	public static async portExists(portNumber: number): Promise<boolean> {
		try {
			let list = child_process.execSync(`mode COM${portNumber}`, execSyncOptions);
			let match = list.match(new RegExp(`COM${portNumber}:`, "g"));
			return match !== null;
		} catch (err) {
			console.error('SerialConnection.portExists failed!');
		}
		return false;
	}

	public clearTerminal(): void {
		this._writeEmitter.fire(TerminalCodes.CLEAR);
		// Move the cursor down to keep the terminal auto-scrolling.
		this._writeEmitter.fire(TerminalCodes.ARROW_DOWN);
	}

	public async open(): Promise<void> {
		if (await SerialConnection.portExists(this._portNumber)) {
			let mode = child_process.execSync(
				`mode COM${this._portNumber}: ` +
				`baud=${this._baud} ` +
				`data=${this._dataBits} ` +
				`stop=${this._stopBits} ` +
				`parity=${this._parity} ` +
				`rts=${this._requestToSend}`,
				execSyncOptions);
			let port = `\\\\.\\COM${this._portNumber}`;
			this._fileDescriptor = fs.openSync(port, 'w+');
			this._isRunning = true;
			setTimeout(this.readInBackground.bind(this), 10);

			this._terminal = vscode.window.createTerminal(this.getExtensionTerminalOptions());
			this._terminal.show(false);
			this._context.subscriptions.push(this._terminal);
		} else {
			vscode.window.showInformationMessage(`Serial Port ${this._portNumber} couldn't be opened!`);
			// throw new Error(`Serial Port ${this._portNumber} doesn't exist`);
		}
	}

	private close(): void {
		this._isRunning = false;
		if (this._fileDescriptor > 0) {
			// fs.closeSync(this._fileDescriptor);
		}
		console.log(`Terminal ${this._portNumber} closed.`);
	}

	public dispose(): void { this.close(); }

	public async write(value: string): Promise<void> {
		// fs.writeSync(this._fileDescriptor, value, null, "ascii");
		fs.write(this._fileDescriptor, value, undefined, "ascii", this.writeCallback.bind(this));
	}

	private readInBackground(): void {
		if (this._isRunning) {
			fs.read(this._fileDescriptor, this._buffer, 0, 1, null, this.readCallback.bind(this));
		}
	}

	private writeCallback<TBuffer extends NodeJS.ArrayBufferView>(
		err: NodeJS.ErrnoException | null,
		written: number,
		str: string
	): void {
	}

	private readCallback<TBuffer extends NodeJS.ArrayBufferView>(
		err: NodeJS.ErrnoException | null,
		bytesRead: number,
		buffer: TBuffer
	): void {
		if (this._isRunning) {
			if (bytesRead >= 1) {
				let data = buffer.toString();
				this._writeEmitter.fire(data);
				setTimeout(this.readInBackground.bind(this), 0);
			} else {
				setTimeout(this.readInBackground.bind(this), 100);
			}
		}
	}
}
