'use strict';

import * as vscode from 'vscode';
import { SerialConnection } from './serial';
import { ControlView, ControlItem } from './controlView';

const languageSelector: vscode.DocumentFilter = { language: 'embedded-basic', scheme: 'file' };

export function activate(context: vscode.ExtensionContext) {

	console.log('Embedded-BASIC extension is now active!');

	const controlView = new ControlView(context);
	vscode.window.registerTreeDataProvider('controlView', controlView);

	vscode.commands.registerCommand('controlView.refreshEntry', () => controlView.refresh());
	vscode.commands.registerCommand('controlView.openEntry', async (node: ControlItem) => await node.openPort());

	context.subscriptions.push(vscode.commands.registerCommand('embeddedBasic.helloWorld', () => {
		vscode.window.showInformationMessage('Hello World from Embedded BASIC!');
	}));
}

export function deactivate() {}
