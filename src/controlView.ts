import * as vscode from 'vscode';
import * as path from 'path';
import { SerialConnection } from './serial';

export class ControlView implements vscode.TreeDataProvider<ControlItem> {

	private _onDidChangeTreeData: vscode.EventEmitter<ControlItem | undefined> = new vscode.EventEmitter<ControlItem | undefined>();
	readonly onDidChangeTreeData: vscode.Event<ControlItem | undefined> = this._onDidChangeTreeData.event;

	private _serialConnectionMap = new Map<number, SerialConnection>();
	public get serialConnectionMap() { return this._serialConnectionMap; };

	public constructor(public readonly context: vscode.ExtensionContext) { }

	refresh(): void { this._onDidChangeTreeData.fire(undefined); }

	getTreeItem(element: ControlItem): vscode.TreeItem { return element; }

	getChildren(element?: ControlItem): Thenable<ControlItem[]> {
		if (element) {
			return Promise.resolve([]);
		} else {
			return Promise.resolve(this.getControlItems());
		}
	}

	private async getControlItems(): Promise<ControlItem[]> {
		let controlItems = [];

		for (let port of await SerialConnection.list()) {
			controlItems.push(new ControlItem(this, port));
		}

		return controlItems;
	}

}

export class ControlItem extends vscode.TreeItem {

	constructor(
		private readonly _controlView: ControlView,
		private readonly _port: number
	) {
		super(`Serial Port ${_port}`, vscode.TreeItemCollapsibleState.None);

		this.tooltip = `Click to open Serial Port ${_port} (COM${_port})`;
		this.description = `COM${_port}`;

		this.command = {
			"title": "Open Serial Port",
			"command": "controlView.openEntry",
			"arguments": [this]
		}

		this.iconPath = {
			light: path.join(__filename, '..', '..', 'resources/light', 'open_in_new.svg'),
			dark: path.join(__filename, '..', '..', 'resources/dark', 'open_in_new.svg')
		};

		this.contextValue = 'dependency';
	}

	public async openPort(): Promise<void> {
		let serialConnection = this._controlView.serialConnectionMap.get(this._port);
		if (!serialConnection) {
			serialConnection = new SerialConnection(this._controlView.context, this._port);
			this._controlView.serialConnectionMap.set(this._port, serialConnection);
		}
		if (serialConnection?.isClosed) { await serialConnection?.open(); }
	}

}
