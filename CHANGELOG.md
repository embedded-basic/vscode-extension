# Change Log

All notable changes to the "embedded-basic" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [Unreleased]

## [0.0.1] - 2021-04-11
### Added

- Syntax highlighting
- Opening and reading of serial ports on Windows
